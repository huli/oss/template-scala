lazy val prelude = RootProject(uri("https://gitlab.com/huli/oss/prelude.git"))
lazy val DEPEND  = RootProject(uri("https://sbt:SECRET@gitlab.com/GROUP/SUBGROUP/DEPEND.git"))

// For local development, comment out the above and in the below, and
// adjust for your dir layout.

// lazy val huli    = "$HOME/huli"
// lazy val GROUP   = "$HOME/GROUP"
// lazy val prelude = RootProject(uri(s"file://${huli}/oss/prelude"))
// lazy val DEPEND  = RootProject(uri(s"file://${GROUP}/SUBGROUP/DEPEND"))

//======================================================================
lazy val TEMPLATE = (project in file("."))
  .dependsOn(prelude % "compile->compile;test->test")
  .dependsOn(DEPEND  % "compile->compile;test->test")
  .settings(

  name         := "TEMPLATE",
  scalaVersion := "2.13.8",

  libraryDependencies ++= Seq(
    "com.github.scalaprops" %% "scalaprops" % "0.9.0",
  ),

  Compile / scalaSource       := baseDirectory.value / "src",
  Test    / scalaSource       := baseDirectory.value / "test",
  Compile / resourceDirectory := baseDirectory.value / "resources",

  console / initialCommands   += "import TEMPLATE._",

  scalacOptions ++= Seq(
    "-encoding", "utf8",      //
    "-explaintypes",          //better type error messages
    "-language:_",            //enable all language features
    "-opt:_",                 //enable all optimizations
    "-Yno-imports",           //do not auto-import scala, java.lang, Predef
    "-deprecation",           //enable deprecation warnings
    "-feature",               //enable feature warnings
    "-unchecked",             //enable unchecked warnings
    "-opt-warnings:_",        //enable optimizer warnings
    "-Xfatal-warnings",       //warnings are errors
    "-Ywarn-dead-code",       //
    "-Ywarn-value-discard",   //
  ),

  Compile / console / scalacOptions --= Seq(
    "-opt:_",
    "-Yno-imports",
    "-Xfatal-warnings",
  ),
)

//======================================================================
scalapropsSettings
scalapropsVersion := "0.9.0"

//======================================================================
//enablePlugins(JmhPlugin)
//lazy val bench = (project in file ("bench"))
//  .dependsOn(TEMPLATE)
//  .enablePlugins(JmhPlugin)
//  .settings(
//    Compile / scalaSource := baseDirectory.value / "src"
//  )

// Build.sbt
